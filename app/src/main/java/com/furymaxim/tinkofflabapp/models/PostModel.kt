package com.furymaxim.tinkofflabapp.models

import com.google.gson.annotations.SerializedName

class PostModel {
    @SerializedName("id")
    var id: Int? = null
    @SerializedName("description")
    var description: String? = null
    @SerializedName("votes")
    var votes: Int? = null
    @SerializedName("author")
    var author: String? = null
    @SerializedName("date")
    var date: String? = null
    @SerializedName("gifURL")
    var gifURL: String? = null
    @SerializedName("gifSize")
    var gifSize: Int? = null
    @SerializedName("previewURL")
    var previewURL: String? = null
    @SerializedName("videoURL")
    var videoURL: String? = null
    @SerializedName("videoSize")
    var videoSize: Int? = null
    @SerializedName("type")
    var type: String? = null
    @SerializedName("width")
    var width: String? = null
    @SerializedName("height")
    var height: String? = null
    @SerializedName("commentsCount")
    var commentsCount: Int? = null
    @SerializedName("fileSize")
    var fileSize: Int? = null
    @SerializedName("canVote")
    var canVote: Boolean? = null
}