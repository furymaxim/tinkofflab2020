package com.furymaxim.tinkofflabapp.ui.fragments


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import android.widget.FrameLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager.widget.ViewPager
import com.furymaxim.tinkofflabapp.ui.fragments.tabs.BestTabFragment
import com.furymaxim.tinkofflabapp.ui.fragments.tabs.HotTabFragment
import com.furymaxim.tinkofflabapp.ui.fragments.tabs.LastTabFragment
import com.furymaxim.tinkofflabapp.R
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.fragment_tab.*


class TabFragment : Fragment(R.layout.fragment_tab) {

    var mTitles = arrayOf("Последние", "Лучшие", "Горячие")
    private var lastSelect = 0

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupTabLayout()

        pager!!.adapter = PagerAdapter(childFragmentManager)
        pager!!.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {
                update(position)
            }

            override fun onPageScrollStateChanged(state: Int) {}
        })
        pager!!.currentItem = lastSelect
    }

    private fun update(position: Int) {
        for (i in mTitles.indices) {

            tabs!!.getTabAt(i)!!.customView!!.findViewById<TextView>(R.id.text).isSelected = false
            tabs!!.getTabAt(i)!!.customView!!.findViewById<TextView>(R.id.text).setTextColor(
                ContextCompat.getColor(context!!,
                    R.color.colorBlack_300
                )
            )

            if (i == position) {
                tabs!!.getTabAt(i)!!.customView!!.findViewById<TextView>(R.id.text).isSelected =
                    true
                tabs!!.getTabAt(i)!!.customView!!.findViewById<TextView>(R.id.text).setTextColor(
                    ContextCompat.getColor(context!!,
                        R.color.primary
                    )
                )
            }
        }
        lastSelect = position
    }

    private fun setupTabLayout() {
        for (current in mTitles.indices) {
            val tab: TabLayout.Tab = tabs!!.newTab()

            tab.setCustomView(R.layout.tabs)

            (tab.customView as FrameLayout).findViewById<TextView>(R.id.text).text =
                mTitles[current]

            tab.customView!!.setOnClickListener { pager!!.currentItem = current }

            tabs!!.addTab(tab)
        }
    }

    override fun onResume() {
        super.onResume()
        update(pager!!.currentItem)
    }

    inner class PagerAdapter(fm: FragmentManager?) :
        FragmentPagerAdapter(fm!!, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

        override fun getPageTitle(position: Int): CharSequence? {
            return mTitles[position]
        }

        override fun getItem(position: Int): Fragment {
            return when (position) {
                0 -> {
                    LastTabFragment.newInstance()
                }
                1 -> {
                    BestTabFragment.newInstance()
                }
                else -> {
                    HotTabFragment.newInstance()
                }
            }
        }

        override fun getCount(): Int {
            return mTitles.size
        }
    }


    companion object {
        fun newInstance() =
            TabFragment()
    }
}
