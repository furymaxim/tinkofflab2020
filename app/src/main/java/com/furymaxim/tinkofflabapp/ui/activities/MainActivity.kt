package com.furymaxim.tinkofflabapp.ui.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.content.ContextCompat
import com.furymaxim.tinkofflabapp.R
import com.furymaxim.tinkofflabapp.ui.fragments.TabFragment

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        window.statusBarColor = ContextCompat.getColor(this,
            R.color.colorPrimary
        )

        setFragment()
    }


    private fun setFragment(){
        supportFragmentManager.beginTransaction()
            .replace(
                R.id.container,
                TabFragment.newInstance()
            )
            .commit()
    }


    override fun onBackPressed() {
        val count = supportFragmentManager.backStackEntryCount

        if (count == 0) {
            super.onBackPressed()
        } else {
            supportFragmentManager.popBackStack()
        }
    }
}
