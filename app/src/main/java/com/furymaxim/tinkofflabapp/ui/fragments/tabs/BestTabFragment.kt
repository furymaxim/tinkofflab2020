package com.furymaxim.tinkofflabapp.ui.fragments.tabs


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.furymaxim.tinkofflabapp.R

/**
 * A simple [Fragment] subclass.
 */
class BestTabFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_best_tab, container, false)
    }

    companion object {
        fun newInstance() =
            BestTabFragment()
    }
}
