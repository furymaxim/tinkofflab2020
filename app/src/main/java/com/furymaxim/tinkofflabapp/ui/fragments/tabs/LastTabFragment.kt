package com.furymaxim.tinkofflabapp.ui.fragments.tabs


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.load.resource.gif.GifDrawable
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.DrawableImageViewTarget
import com.bumptech.glide.request.target.Target
import com.furymaxim.tinkofflabapp.R
import com.furymaxim.tinkofflabapp.models.PostModel
import com.furymaxim.tinkofflabapp.network.NetworkService
import com.furymaxim.tinkofflabapp.utils.Utils
import kotlinx.android.synthetic.main.fragment_last_tab.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LastTabFragment : Fragment(R.layout.fragment_last_tab) {

    var currentPos = 0
    var maxPos = 0
    val arrayList = ArrayList<PostModel>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        previous.setImageDrawable(resources.getDrawable(R.drawable.ic_arrow_back_not_active_48dp, null))

        if(Utils.isNetworkAvailable(context)){
            img.setImageDrawable(null)
            descriptionTV.text = ""
            getPost()
        } else {
            img.setImageDrawable(resources.getDrawable(R.drawable.ic_no_connection, null))
            descriptionTV.text = "Проверьте интернет-соединение"
            next.setOnClickListener(null)
            previous.setOnClickListener(null)
            progressbar.visibility = View.GONE
            next.setImageDrawable(resources.getDrawable(R.drawable.ic_arrow_forward_not_active_48dp, null))
        }

        next.setOnClickListener {
            currentPos += 1
            //Toast.makeText(context, "curPos $currentPos maxPos $maxPos",Toast.LENGTH_SHORT).show()
            previous.setImageDrawable(resources.getDrawable(R.drawable.ic_arrow_back_48dp, null))
            previous.isClickable = true
            if(Utils.isNetworkAvailable(context)){
                progressbar.visibility = View.VISIBLE
                if(currentPos > maxPos) {
                    getPost()
                } else {
                    setupFields(arrayList[currentPos].description!!, arrayList[currentPos].gifURL!!)
                }
            } else {
                if(maxPos >= currentPos) {
                    setupFields(arrayList[currentPos].description!!, arrayList[currentPos].gifURL!!)
                } else {
                    img.setImageDrawable(resources.getDrawable(R.drawable.ic_no_connection, null))
                    descriptionTV.text = "Проверьте интернет-соединение"
                    next.setOnClickListener(null)
                    progressbar.visibility = View.GONE
                    next.setImageDrawable(
                        resources.getDrawable(
                            R.drawable.ic_arrow_forward_not_active_48dp,
                            null
                        )
                    )
                }
            }
            if(currentPos - 1 == maxPos) {
                maxPos += 1
            }
        }

        previous.setOnClickListener {
            currentPos -= 1
            //Toast.makeText(context, "curPos $currentPos maxPos $maxPos",Toast.LENGTH_SHORT).show()
            progressbar.visibility = View.VISIBLE
            if(currentPos == 0) {
                previous.setImageDrawable(resources.getDrawable(R.drawable.ic_arrow_back_not_active_48dp, null))
                previous.isClickable = false
            }
            setupFields(arrayList[currentPos].description!!, arrayList[currentPos].gifURL!!)
        }
    }

    private fun getPost(){
        NetworkService.getInstance()
            .getJSONApi()
            .getPost("true")
            .enqueue(object: Callback<PostModel>{
                override fun onFailure(call: Call<PostModel>, t: Throwable) {
                    call.cancel()
                    img.setImageDrawable(resources.getDrawable(R.drawable.ic_error, null))
                    descriptionTV.text = "Ошибка при загрузке поста"
                }

                override fun onResponse(call: Call<PostModel>, response: Response<PostModel>) {
                    if(response.isSuccessful){
                        val post = response.body()

                        val gifImgLink = post!!.gifURL
                        val description = post.description

                        arrayList.add(post)

                        setupFields(description!!, gifImgLink!!)
                        call.cancel()
                    }
                }
            })
    }


    private fun setupFields(description: String, gifImgLink: String){
        val target = DrawableImageViewTarget(img)

        descriptionTV.text = description

        Glide.with(context!!).asGif().transition(withCrossFade())
            .load(gifImgLink)
            .listener(object : RequestListener<GifDrawable> {
                override fun onLoadFailed(
                    e: GlideException?,
                    model: Any?,
                    target: Target<GifDrawable>?,
                    isFirstResource: Boolean
                ): Boolean {
                    e!!.printStackTrace()
                    progressbar.visibility = View.GONE
                    return false
                }

                override fun onResourceReady(
                    resource: GifDrawable?,
                    model: Any?,
                    target: Target<GifDrawable>?,
                    dataSource: DataSource?,
                    isFirstResource: Boolean
                ): Boolean {
                    progressbar.visibility = View.GONE
                    return false
                }
            })
            .into(target.view)
    }

    companion object {
        fun newInstance() =
            LastTabFragment()
    }

}
