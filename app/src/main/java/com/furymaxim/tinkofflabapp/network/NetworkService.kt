package com.furymaxim.tinkofflabapp.network

import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class NetworkService{
    private var mRetrofit: Retrofit? = null
    //private val context = MyApplication.appContext

    init{
        val client = OkHttpClient.Builder()

        client.networkInterceptors().add(CacheInterceptor())
        /*val httpCacheDirectory = File(context.cacheDir, "responses")
        val cacheSize = 50L * 1024 * 1024
        val cache = Cache(httpCacheDirectory, cacheSize)*/

        //client.cache(cache)
        mRetrofit = Retrofit.Builder()
            .baseUrl(Conf.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(client.build())
            .build()
    }

    fun getJSONApi(): APIInterface {
        return mRetrofit!!.create(APIInterface::class.java)
    }

    companion object{
        private var mInstance: NetworkService? = null

        /*private val REWRITE_CACHE_CONTROL_INTERCEPTOR = Interceptor {
            val originalResponse = it.proceed(it.request())

            val context = MyApplication.appContext


            if(Utils.isNetworkAvailable(context)){
                val maxAge = 3600

                originalResponse.newBuilder()
                    .removeHeader("Pragma")
                    .header("Cache-Control", "public, max-age=" + maxAge)
                    .build()


            } else {
                val maxStale = 60 * 60 * 24 * 28 // tolerate 4-weeks stale
                originalResponse.newBuilder()
                    .removeHeader("Pragma")
                    .header("Cache-Control", "public, only-if-cached, max-stale=$maxStale")
                    .build()
            }

        }
        */

        fun getInstance(): NetworkService {
            if (mInstance == null) mInstance =
                NetworkService()

            return mInstance!!
        }
    }




}