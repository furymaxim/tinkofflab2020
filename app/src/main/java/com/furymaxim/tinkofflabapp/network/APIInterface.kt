package com.furymaxim.tinkofflabapp.network

import com.furymaxim.tinkofflabapp.models.PostModel
import retrofit2.Call
import retrofit2.http.*

interface APIInterface {

    @GET("random")
    fun getPost(@Query("json") json:String):Call<PostModel>
}